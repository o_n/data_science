%let indir = /folders/myfolders/data_science/csv/sample-data/section8/daily/;

* データ読込 ;
%macro read_dau(from_ymd=, to_ymd=);

    data _null_;
        do i = 0 to input("&to_ymd.", yymmdd8.) - input("&from_ymd.", yymmdd8.);
            day1 = put(intnx("day", input("&from_ymd.", yymmdd8.), i), yymmdd10.);
            day2 = put(intnx("day", input("&from_ymd.", yymmdd8.), i), yymmddn8.);
            call symputx("day1_" || compress(put(i, best.)), day1);
            call symputx("day2_" || compress(put(i, best.)), day2);
        end;
        call symputx("days", i - 1);
    run;

    %do i = 0 %to &days.;
        data dau_&&day2_&i..;
            infile "&indir.dau/game-01/&&day1_&i../data.tsv" dsd dlm = "09"x missover firstobs = 2 lrecl = 32767;
            attrib log_date length = $10 label = "年月日"
                   app_name length = $10 label = "ゲーム名"
                   user_id  length = 8   label = "ユーザID"
            ;
            input log_date app_name user_id;
        run;
    %end;

%mend read_dau;

%read_dau(from_ymd=20130501, to_ymd=20131031);


%macro read_dpu(from_ymd=, to_ymd=);

    data _null_;
        do i = 0 to input("&to_ymd.", yymmdd8.) - input("&from_ymd.", yymmdd8.);
            day1 = put(intnx("day", input("&from_ymd.", yymmdd8.), i), yymmdd10.);
            day2 = put(intnx("day", input("&from_ymd.", yymmdd8.), i), yymmddn8.);
            call symputx("day1_" || compress(put(i, best.)), day1);
            call symputx("day2_" || compress(put(i, best.)), day2);
        end;
        call symputx("days", i - 1);
    run;

    %do i = 0 %to &days.;
        data dpu_&&day2_&i..;
            infile "&indir.dpu/game-01/&&day1_&i../data.tsv" dsd dlm = "09"x missover firstobs = 2 lrecl = 32767;
            attrib log_date length = $10 label = "年月日"
                   app_name length = $10 label = "ゲーム名"
                   user_id  length = 8   label = "ユーザID"
                   payment  length = 8   label = "課金額"
            ;
            input log_date app_name user_id payment;
        run;
    %end;

%mend read_dpu;

%read_dpu(from_ymd=20130501, to_ymd=20130503);


