%let indir = /folders/myfolders/data_science/csv/;

* データ読込 ;
data ad_result;
    infile "&indir.ad_result.csv" dsd missover firstobs = 2 lrecl = 32767;
    attrib month    length = 8 label = "表示日付" informat = anydtdte. format = yymmn6.
           tvcm     length = 8 label = "TV広告"
           magazine length = 8 label = "雑誌広告"
           install  length = 8 label = "新規インストール数"
    ;
    input month tvcm magazine install;
run;
