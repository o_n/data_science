data ad_result2;
    set ad_result;
    cpi = sum(tvcm, magazine) / install;
run;

title "パズコレのマスメディア広告の時系列CPI(Cost Per Install)";
proc sgplot data = ad_result2;
    vbar month / response = cpi;
run;


* 06-02 ;
title "TV広告と新規ユーザ数";
proc sgplot data = ad_result2;
    scatter x = tvcm y = install;
run;


* 06-03 ;
title "雑誌広告と新規ユーザ数";
proc sgplot data = ad_result2;
    scatter x = magazine y = install;
run;


proc reg data = ad_result;
    model install = tvcm magazine;
    *print stb r;
run;

