* データ加工 ;
proc sort data = ab_test_imp out = ab_test_imp2;
    by transaction_id;
run;

proc sort data = ab_test_goal out = ab_test_goal2;
    by transaction_id;
run;

data ab_test_imp3;
    merge ab_test_imp2 (in=in1)
          ab_test_goal2(in=in2 keep=transaction_id)
    ;
    by transaction_id;
    if in1;
    if in1 and in2 then is_goal = 1;
    else                is_goal = 0;
run;


* 05-03 05-04 ;
proc freq data = ab_test_imp3;
    tables test_case * is_goal / missing nocol nopercent chisq;
run;

proc sgplot data = ab_test_imp3;
    vbar test_case / group = is_goal;
run;


* データ加工 ;
proc sort data = ab_test_imp out = ab_test_imp4;
    by test_case log_date;
run;

proc freq data = ab_test_imp4;
    tables test_case * log_date / missing noprint out = ab_test_imp5(drop=percent rename=(count=imp));
run;

proc sort data = ab_test_goal out = ab_test_goal4;
    by test_case log_date;
run;

proc freq data = ab_test_goal4;
    tables test_case * log_date / missing noprint out = ab_test_goal5(drop=percent rename=(count=cv));
run;

data ab_test_imp6;
    merge ab_test_imp5 (in=in1)
          ab_test_goal5(in=in2)
    ;
    by test_case log_date;
    cvr = cv / imp;
run;

proc summary data = ab_test_imp6;
    by test_case;
    var cv imp;
    output out = ab_test_imp7(drop=_type_ _freq_) sum = ;
run;

data _null_;
    set ab_test_imp7;
    cvr_avg = cv / imp;
    call symput("cvr_avg_" || test_case, compress(put(cvr_avg, best.)));
run;

data ab_test_imp8;
    set ab_test_imp6;
    if      test_case eq "A" then cvr_avg = &cvr_avg_A;
    else if test_case eq "B" then cvr_avg = &cvr_avg_B;
run;


* 05-06 ;
proc sgplot data = ab_test_imp8;
    series x = log_date y = cvr / group = test_case;
    series x = log_date y = cvr_avg / group = test_case;
run;




