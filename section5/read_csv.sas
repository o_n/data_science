%let indir = /folders/myfolders/data_science/csv/;

* データ読込 ;
data ab_test_imp;
    infile "&indir.section5-ab_test_imp.csv" dsd missover firstobs = 2 lrecl = 32767;
    attrib log_date       length = 8   label = "表示日付" informat = yymmdd. format = yymmdds10.
           app_name       length = $10 label = "アプリ名"
           test_name      length = $20 label = "テスト名"
           test_case      length = $1  label = "テストケース"
           user_id        length = 8   label = "ユーザID"
           transaction_id length = 8   label = "トランザクションID"
    ;
    input log_date app_name test_name test_case user_id transaction_id;
run;

data ab_test_goal;
    infile "&indir.section5-ab_test_goal.csv" dsd missover firstobs = 2 lrecl = 32767;
    attrib log_date       length = 8   label = "表示日付" informat = yymmdd. format = yymmdds10.
           app_name       length = $10 label = "アプリ名"
           test_name      length = $20 label = "テスト名"
           test_case      length = $1  label = "テストケース"
           user_id        length = 8   label = "ユーザID"
           transaction_id length = 8   label = "トランザクションID"
    ;
    input log_date app_name test_name test_case user_id transaction_id;
run;

