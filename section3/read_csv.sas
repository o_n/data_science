%let indir = /folders/myfolders/data_science/csv/;

* データ読込 ;
data dau;
    infile "&indir.section3-dau.csv" dsd missover firstobs = 2 lrecl = 32767;
    attrib log_date length = 8   label = "基準日" informat = yymmdd. format = yymmdds10.
           app_name length = $10 label = "アプリ名"
           user_id  length = 8   label = "ユーザID"
    ;
    input log_date app_name user_id;
run;

data dpu;
    infile "&indir.section3-dpu.csv" dsd missover firstobs = 2 lrecl = 32767;
    attrib log_date length = 8   label = "基準日" informat = yymmdd. format = yymmdds10.
           app_name length = $10 label = "アプリ名"
           user_id  length = 8   label = "ユーザID"
           payment  length = 8   label = "課金額"
    ;
    input log_date app_name user_id payment;
run;

data install;
    infile "&indir.section3-install.csv" dsd missover firstobs = 2 lrecl = 32767;
    attrib install_date length = 8   label = "利用開始日" informat = yymmdd. format = yymmdds10.
           app_name     length = $10 label = "アプリ名"
           user_id      length = 8   label = "ユーザID"
    ;
    input install_date app_name user_id;
run;

