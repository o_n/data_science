* データチェックいろいろ ;

proc contents data = dau;
run;

proc contents data = dpu;
run;

proc contents data = install;
run;


* アクセス日とアプリ名の度数集計 ;
proc freq data = dau;
    tables log_date app_name / missing nocol norow nopercent;
run;
/*
アクセス日：2013/06/01～2013/07/31
アプリ名：game-01
*/

* ユニークキーの確認 ;
proc sort data = dau out = __m1 nodupkey;
    by log_date user_id;
run;
/* log_date user_idで一意になっている模様 */

* アクセス日毎のアクセス数の推移 ;
proc sgplot data = dau;
    histogram log_date;
run;

