proc contents data = dpu;
run;

proc sort data = dpu out = __m1;
    by log_date user_id;
run;

proc sgplot data = dpu;
    histogram log_date;
run;

* 月ごとに集計するための変数を作成 ;
data dpu2;
    set dpu;
    ym = put(log_date, yymmn6.);
run;

proc freq data = dpu2;
    tables ym / missing nocol norow nopercent;
    *weight payment;
run;

proc sort data = dpu2 out = dpu3;
    by ym user_id;
run;

proc summary data = dpu3;
    var payment;
    by ym user_id;
    output out = __m2(drop=_type_) sum = ;
run;

* 各月の課金回数上位5名 ;
proc sort data = __m2 out = __m3;
    by ym descending _freq_;
run;

proc print data = __m3(obs=5);
    where ym eq "201306";
run;

proc print data = __m3(obs=5);
    where ym eq "201307";
run;

* 各月の課金金額上位5名 ;
proc sort data = __m2 out = __m4;
    by ym descending payment;
run;

proc print data = __m4(obs=5);
    where ym eq "201306";
run;

proc print data = __m4(obs=5);
    where ym eq "201307";
run;

* 月別課金総額 ;
proc freq data = dpu2;
    tables ym / missing nocol norow nopercent;
    weight payment;
run;

proc sgplot data = dpu2;
    vbar ym / freq = payment;
run;


* installデータのユニークチェック ;
proc sort data = install out = __m1 nodupkey;
    by user_id;
run;
* user_id で一意 ;

* 日次の新規ユーザ数 ;
proc sgplot data = install;
    histogram install_date;
run;
/*
5月頭に突出して新規ユーザ数の多い日がある
7月後半に落ち込みが見られる
*/

* dau unique check ;
proc sort data = dau out = __m1 nodupkey;
    by log_date user_id;
run;

* app_name check ;
proc freq data = dau;
    tables app_name / missing nocol norow nopercent;
run;

proc freq data = dpu;
    tables app_name / missing nocol norow nopercent;
run;

proc freq data = install;
    tables app_name / missing nocol norow nopercent;
run;
* 3データ共に、game-01のデータのみ ;

* テキストとは別のやり方 ;
proc sort data = dau(drop=app_name) out = dau2;
    by log_date user_id;
run;

proc sort data = dpu(drop=app_name) out = dpu2;
    by log_date user_id;
run;

proc summary data = dpu2;
    by log_date user_id;
    var payment;
    output out = dpu3(drop=_type_ _freq_) sum = ;
run;

data dau3;
    merge dau2(in=in1)
          dpu3(in=in2)
    ;
    by log_date user_id;
    if in1;
    log_ym = put(log_date, yymmn6.);
run;

proc sort data = dau3 out = dau4;
    by log_ym user_id;
run;

proc summary data = dau4;
    by log_ym user_id;
    var payment;
    output out = dau5(drop=_type_ _freq_) sum = ;
run;

proc sort data = dau5 out = dau6;
    by user_id;
run;

data install2(drop=install_date);
    set install(drop=app_name);
    install_ym = put(install_date, yymmn6.);
run;

proc sort data = install2 out = install3;
    by user_id;
run;

data dau7;
    merge dau6    (in=in1)
          install3(in=in2)
    ;
    by user_id;
    if in1;
    attrib new_flg length = $10 label = "新規／既存";
    if log_ym eq install_ym then new_flg = "新規";
    else                         new_flg = "既存";
run;

proc sort data = dau7 out = __m1;
    by log_ym new_flg;
run;

proc summary data = __m1;
    by log_ym new_flg;
    var payment;
    output out = __m2 sum = ;
run;

* 別解 ;
proc freq data = dau7;
    tables log_ym * new_flg / missing nocol norow nopercent;
    weight payment;
run;

* P48 積み上げ棒グラフ ;
proc sgplot data = dau7;
    vbar log_ym / freq = payment grouporder = ascending group = new_flg;
run;

data dau8;
    set dau7;
    pay2000 = int(payment/2000)*2000;
run;

* P49 新規ユーザのパズコレの売上比較(先月／今月) ;
proc sgplot data = dau8(where=(new_flg eq "新規"));
    vbar pay2000 / group = log_ym groupdisplay = cluster barwidth = 1;
run;


