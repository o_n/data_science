* データ加工 ;
proc sort data = dau out = dau2;
    by user_id;
run;

proc sort data = user_info out = user_info2;
    by user_id;
run;

data dau3;
    merge dau2      (in=in1)
          user_info2(in=in2)
    ;
    by user_id;
    if in1;
    log_ym = put(log_date, yymmn6.);
run;


* 04-03 ;
title "セグメント分析(性別)";
proc freq data = dau3;
    tables log_ym * gender / missing nocol norow nopercent;
run;

proc sgplot data = dau3;
    vbar gender / group = log_ym groupdisplay = cluster;
run;


* 04-04 ;
title "セグメント分析(年代)";
proc freq data = dau3;
    tables log_ym * generation / missing nocol norow nopercent;
run;

proc sgplot data = dau3;
    vbar generation / group = log_ym groupdisplay = cluster;
run;


* 04-05 ;
title "セグメント分析(性別×年代)";
proc freq data = dau3;
    tables gender * log_ym * generation / missing nocol norow nopercent;
run;

proc sgpanel data = dau3;
    panelby gender;
    vbar generation / group = log_ym groupdisplay = cluster;
run;


* 04-06 ;
title "セグメント分析(デバイスごと)";
proc freq data = dau3;
    tables log_ym * device_type / missing nocol norow nopercent;
run;

proc sgplot data = dau3;
    vbar device_type / group = log_ym groupdisplay = cluster;
run;


* 04-07 ;
proc freq data = dau3;
    tables log_date * device_type / missing out = dau4 noprint;
run;

title "デバイス別UU(Unique user:ユニークユーザ)推移";
proc sgplot data = dau4;
    series x = log_date y = count / group = device_type;
run;
title;





