%let indir = /folders/myfolders/data_science/csv/;

* データ読込 ;
data dau;
    infile "&indir.section4-dau.csv" dsd missover firstobs = 2 lrecl = 32767;
    attrib log_date length = 8   label = "アクセス日" informat = yymmdd. format = yymmdds10.
           app_name length = $10 label = "アプリ名"
           user_id  length = 8   label = "ユーザID"
    ;
    input log_date app_name user_id;
run;

data user_info;
    infile "&indir.section4-user_info.csv" dsd missover firstobs = 2 lrecl = 32767;
    attrib install_date length = 8   label = "利用開始日" informat = yymmdd. format = yymmdds10.
           app_name     length = $10 label = "アプリ名"
           user_id      length = 8   label = "ユーザID"
           gender       length = $1  label = "性別"
           generation   length = 8   label = "年代"
           device_type  length = $10 label = "デバイスの種類"
    ;
    input install_date app_name user_id gender generation device_type;
run;

